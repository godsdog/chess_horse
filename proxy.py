# File: simplehttpserver-example-2.py

# a truly minimal HTTP proxy

import SocketServer
import SimpleHTTPServer
import urllib

import socket, thread, select

__version__ = '0.1.0 Draft 1'
BUFLEN = 8192
VERSION = 'Python Proxy/'+__version__
HTTPVER = 'HTTP/1.1'

PORT = 1235


class Proxy(SimpleHTTPServer.SimpleHTTPRequestHandler):
    # def _set_headers(self):
    #     self.send_response(200)
    #     self.send_header('Accept', 'application/json')
    #     self.send_header('XXX', 'yyy')
    #     print('............')
    #     self.end_headers()

    def method_CONNECT(self):
        self._connect_target(self.path)
        print('aaaaaa')
        self.client.send(HTTPVER+' 200 Connection established\n'+
                         'Proxy-agent: %s\n\n'%VERSION)
        self.client_buffer = ''
        self._read_write()

    def do_GET(self):
        print('sdsdsdsdssd')
        self.copyfile(urllib.urlopen(self.path), self.wfile)


httpd = SocketServer.ForkingTCPServer(('', PORT), Proxy)
print("serving at port", PORT)
httpd.serve_forever()
