#!/usr/bin/python
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import SimpleHTTPServer
import urllib
import ssl

PORT_NUMBER = 1234

# TODO: working version only for http requests


# This class will handles any incoming request from
# the browser
# class myHandler(BaseHTTPRequestHandler, SimpleHTTPServer.SimpleHTTPRequestHandler):
class myHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):

    # Handler for the GET requests
    def do_GET(self):
        self.send_response(200)
        self.send_header('Accept', 'application/json')
        self.end_headers()
        # Send the html message
        self.copyfile(urllib.urlopen(self.path), self.wfile)
        # self.wfile.write("Hello World !")
        return    # Handler for the GET requests


try:
    # Create a web server and define the handler to manage the
    # incoming request
    server = HTTPServer(('', PORT_NUMBER), myHandler)
    server.socket = ssl.wrap_socket(server.socket, certfile='/home/andriy/PROJECTS/chess/new.cert.cert',
                                    keyfile='/home/andriy/PROJECTS/chess/new.cert.key', server_side=True)
    print 'Started httpserver on port ', PORT_NUMBER

    # Wait forever for incoming http requests
    server.serve_forever()

except KeyboardInterrupt:
    print '^C received, shutting down the web server'
    server.socket.close()
