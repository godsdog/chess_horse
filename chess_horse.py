#!/usr/local/bin/pypy/bin/pypy3
from datetime import datetime
STEPS = [(-1, -2), (-1, 2), (1, -2), (1, 2), (-2, -1), (-2, 1), (2, -1), (2, 1)]

t1 = datetime.now()


class Path(object):
    def __init__(self, path, size):
        self.path = path
        self.size = size

    def step_forward(self, step):
        x = (self.path[-1][0] + STEPS[step][0], self.path[-1][1] + STEPS[step][1])
        if 0 < x[0] <= self.size[0] and 0 < x[1] <= self.size[1] and x not in self.path:
            self.path.append(x)
            return True
        return False


def chessboard(init_path=(1, 1), size=(8, 8)):
    LEN = size[0] * size[1]
    path = Path([init_path], size)
    len_steps = len(STEPS)

    def path_finder(path):
        for step in range(len_steps):
            if path.step_forward(step):
                path_finder(path)
        if len(path.path) == LEN:
            return path.path

        path.path.pop()
        return

    return path_finder(path)


path = chessboard()

t2 = datetime.now()
dt = t2 - t1
print(dt)

print(str(path))
print(len(path))
